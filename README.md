# Pose estimation
This repository contains an implementation of [pose-estimation from Mediapipe](https://google.github.io/mediapipe/solutions/pose.html) in Javascript. Pose-estimation is done using GPU acceleration in the web-browser and in this case is used to detect if the detected person is in the correct pose to apply pose transfer. This means standing straight without any crossing arms/legs. 

# How to start
Note that I never used NodeJS/Javascript before so this might not be the recommended way :)  All the dependencies should be included in the `package.json` which need to be installed. For the webserver I used [express](https://www.npmjs.com/package/express) which can be started with `node index.js` while in the `src` folder. 

# How to use
When going to `localhost:5000` a check will be done on your webbrowser. Chrome (based) is recommended for best performance and if something else is used a notification is shown. Camera access is requested after which Mediapipe is loaded. Pose-tracking should start immediately and a couple of parameters can be tuned:

* Model complexity: various models are available which should result in varying performance. I checked and did not see much of a difference both performance wise and accuracy wise though I did not experiment very extensivly.
* min_detection_confidence: minimum confidence value (`[0.0, 1.0]`) from the person-detection model for the detection to be considered successful. Default to `0.5`.
* min_tracking_confidence: minimum confidence value (`[0.0, 1.0]`) from the landmark-tracking model for the pose landmarks to be considered tracked successfully, or otherwise person detection will be invoked automatically on the next input image.

Defaults should be okay although `min_detection_confidence` can be fine-tuned depending on the setting.

## Pose detection
As stated above pose tracking is done by default however we want to classify if a person is standing straight, without crossing arms or legs. First a check is done if all possible keypoints are visible in the frame. If so we check the pose. To do this a simple function `determine_correct_pose` is available which checks the order of keypoints based on [the Google documentation](https://google.github.io/mediapipe/solutions/pose.html). If a correct pose is detected the ML6 logo in the bottom right will turn green, if not it is red.

# Results
Below is a beautiful comparison on stock photos (photo upload is supported in the web-interface). Note the colour of the ML6 logo which changes based on a correct/incorrect pose.
![Comparison](img/pose-comp.png)