import DeviceDetector from "https://cdn.skypack.dev/device-detector-js@2.2.10";
// Usage: testSupport({client?: string, os?: string}[])
// Client and os are regular expressions.
// See: https://cdn.jsdelivr.net/npm/device-detector-js@2.2.10/README.md for
// legal values for client and os
testSupport([
    { client: 'Chrome' },
]);
function testSupport(supportedDevices) {
    const deviceDetector = new DeviceDetector();
    const detectedDevice = deviceDetector.parse(navigator.userAgent);
    let isSupported = false;
    for (const device of supportedDevices) {
        if (device.client !== undefined) {
            const re = new RegExp(`^${device.client}$`);
            if (!re.test(detectedDevice.client.name)) {
                continue;
            }
        }
        if (device.os !== undefined) {
            const re = new RegExp(`^${device.os}$`);
            if (!re.test(detectedDevice.os.name)) {
                continue;
            }
        }
        isSupported = true;
        break;
    }
    if (!isSupported) {
        alert(`This demo, running on ${detectedDevice.client.name}/${detectedDevice.os.name}, ` +
            `is not well supported at this time. Please use Google Chrome for improved performance.`);
    }
}
const controls = window;
const drawingUtils = window;
const mpPose = window;
const options = {
    locateFile: (file) => {
        return `https://cdn.jsdelivr.net/npm/@mediapipe/pose@${mpPose.VERSION}/${file}`;
    }
};

// Our input frames will come from here.
const videoElement = document.getElementsByClassName('input_video')[0];
const canvasElement = document.getElementsByClassName('output_canvas')[0];
const controlsElement = document.getElementsByClassName('control-panel')[0];
const canvasCtx = canvasElement.getContext('2d');
// We'll add this to our control panel later, but we'll save it here so we can
// call tick() each time the graph runs.
const fpsControl = new controls.FPS();
// Optimization: Turn off animated spinner after its hiding animation is done.
const spinner = document.querySelector('.loading');
spinner.ontransitionend = () => {
    spinner.style.display = 'none';
};

// Simple function changing the colour of the ML6 logo based on correct posture
const logoElement = document.getElementById("logo");
function change_color(visible) {   
    if (visible) {
        logoElement.style.color = "green"
    } else {
        logoElement.style.color = "red"
    }
}

//Determine pose based on https://google.github.io/mediapipe/solutions/pose.html
function determine_correct_pose(points) {

    // Classify arm order, arm, body, body, arm
    let arm_order = points[14]["x"] < points[12]["x"] < points[11]["x"] < points[13]["x"] &&
    points[16]["x"] < points[12]["x"] < points[11]["x"] < points[15]["x"]

    // Arms facing downwards
    let arms_down = points[16]["y"] < points[14]["y"] < points[12]["y"] &&
                points[15]["y"] < points[13]["y"] < points[11]["y"]

    // Classify leg
    let leg_order = points[26]["x"] < points[25]["x"]

    // Standing straight
    let straight_body = points[30]["y"] < points[26]["y"] < points[24]["y"] < points[12]["y"] < points[6]["y"] &&
                points[29]["y"] < points[25]["y"] < points[23]["y"] < points[11]["y"] < points[3]["y"]

    // Everything has to be true for a correct pose
    return arm_order && arms_down && leg_order && straight_body
}


// Determine if you full body is detected using visibility of all points. Total can be 32
function determine_visibility(points, threshold) {
    let total_visibility = 0;
    points.map(point => total_visibility += point["visibility"]);
    return total_visibility > threshold;
}

function onResults(results) {
    // Hide the spinner.
    document.body.classList.add('loaded');
    // Update the frame rate.
    fpsControl.tick();
    // Draw the overlays.
    canvasCtx.save();
    canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    canvasCtx.drawImage(results.image, 0, 0, canvasElement.width, canvasElement.height);
    if (results.poseLandmarks) {
        drawingUtils.drawConnectors(canvasCtx, results.poseLandmarks, mpPose.POSE_CONNECTIONS, { visibilityMin: 0.65, color: 'white' });
        drawingUtils.drawLandmarks(canvasCtx, Object.values(mpPose.POSE_LANDMARKS_LEFT)
            .map(index => results.poseLandmarks[index]), { visibilityMin: 0.65, color: 'white', fillColor: 'rgb(255,138,0)' });
        drawingUtils.drawLandmarks(canvasCtx, Object.values(mpPose.POSE_LANDMARKS_RIGHT)
            .map(index => results.poseLandmarks[index]), { visibilityMin: 0.65, color: 'white', fillColor: 'rgb(0,217,231)' });
        drawingUtils.drawLandmarks(canvasCtx, Object.values(mpPose.POSE_LANDMARKS_NEUTRAL)
            .map(index => results.poseLandmarks[index]), { visibilityMin: 0.65, color: 'white', fillColor: 'white' });
    }
    
    // Compute correct pose, completely visible, standing straight
    let correct_pose = false
    if (results.poseLandmarks) {
        let visibility = determine_visibility(results.poseLandmarks, 31); // Tweak threshold if needed
        if (visibility){
            correct_pose = determine_correct_pose(results.poseLandmarks)
        }        
    }
    change_color(correct_pose)

    canvasCtx.restore();   
}
const pose = new mpPose.Pose(options);
pose.onResults(onResults);
pose.initialize().then( () => {
    // Present a control panel through which the user can manipulate the solution
    // options.
    new controls
        .ControlPanel(controlsElement, {
        modelComplexity: 1,
        smoothLandmarks: true,
        minDetectionConfidence: 0.5,// Minimum confidence value ([0.0, 1.0]) from the person-detection model for the detection to be considered successful. Default to 0.5.
        minTrackingConfidence: 0.5, //Minimum confidence value ([0.0, 1.0]) from the landmark-tracking model for the pose landmarks to be considered tracked successfully, or otherwise person detection will be invoked automatically on the next input image. 
    })
        .add([
        new controls.StaticText({ title: 'MediaPipe Pose' }),
        fpsControl,
        new controls.SourcePicker({
            onSourceChanged: () => {
                // Resets because this model gives better results when reset between
                // source changes.
                pose.reset();
            },
            onFrame: async (input, size) => {
                const aspect = size.height / size.width;
                let width, height;
                if (window.innerWidth > window.innerHeight) {
                    height = window.innerHeight;
                    width = height / aspect;
                }
                else {
                    width = window.innerWidth;
                    height = width * aspect;
                }
                canvasElement.width = width;
                canvasElement.height = height;
                await pose.send({ image: input });
            },
        }),
        new controls.Slider({
            title: 'Model Complexity',
            field: 'modelComplexity',
            discrete: ['Lite', 'Full', 'Heavy'],
        }),
        new controls.Slider({
            title: 'Min Detection Confidence',
            field: 'minDetectionConfidence',
            range: [0, 1],
            step: 0.01
        }),
        new controls.Slider({
            title: 'Min Tracking Confidence',
            field: 'minTrackingConfidence',
            range: [0, 1],
            step: 0.01
        }),
    ])
        .on(x => {
        const options = x;
        pose.setOptions(options);
    });
});